I can barely believe it, I actually set up my website. Now it isn't just a blank
page that says "there's nothing here", now it's a fancy page that says "there's
nothing here".

There. Hello, world.

The delay, of course, was because I am really not a web dev person.
I originally wanted to use some kind of "easy formatter" like
[Jekyll](https://jekyllrb.com/) and [Hugo](https://gohugo.io/). There were
_ok_, but I wanted pleased with some of the side effects, notably my worry
about bringing in extra bloat. Eventually I decided I should just write things
from scratch myself.

I knew I wanted to write blog posts in Markdown. It's just easier. I also
wanted static pages, since I didn't want to write (or setup) a backend system
to generate blog pages. Not that it's a bad idea, I just didn't want to. So I
wrote a [small Python script](https://gitlab.com/ammonsmith.me/blog/blob/master/generate.py) that took Markdown
sources and some basic HTML template files and created individual blog posts
and a simple index. I found the [Python Markdown](https://python-markdown.github.io/)
library to be quite nice, as it had a lot of extensions I was used to using,
and has some nice conversions for things like fancy quotes.

As for the actual website, thankfully it wasn't too bad to do. I looked at
[raylu's](https://blog.raylu.net/style.css) CSS, and after a bit of Googling I
was able to come up with something I liked in a reasonable amount of time.

And I quite like it. I'm in control of all the code on my website, and it's
fairly lightweight. (The biggest source of bloat are the [Google
Fonts](https://fonts.google.com) I'm pulling in, but they're just too pretty to pass up).

I plan to post here more in the future.
