## Blog
Contains blog entries, as well as a small "build" system to create HTML files.

Requires Python 3.5+.

Setup:
```sh
$ pip3 install -r requirements.txt
```

Usage:
```sh
$ python3 generate.py
$ cp -a output/* /path/to/html/server
```
