#!/usr/bin/env python3

#
# generate.py
#
# blog - Easily generate blog HTML
# Copyright (c) 2018 Ammon Smith
#
# blog is available free of charge under the terms of the MIT
# License. You are free to redistribute and/or modify it under those
# terms. It is distributed in the hopes that it will be useful, but
# WITHOUT ANY WARRANTY. See the LICENSE file for more details.
#

from collections import defaultdict
from math import ceil
import argparse
import itertools
import os
import re
import sys

import markdown
import yaml

# Configurable constants
BLOG_ROOT = '/blog'

INPUT_DIR = 'posts'
OUTPUT_DIR = 'output'

INDEX_FILENAME = 'index.html'
INDEX_OUTPUT_FILENAME = 'index-{}.html'
INDEX_NUMBER_TEMPLATE = '<tt>{}</tt>'

INDEX_TEMPLATE_HTML_PATH = 'index-template.html'
ENTRY_TEMPLATE_HTML_PATH = 'entry-template.html'
POST_TEMPLATE_HTML_PATH = 'post-template.html'

INDEX_SIZE = 20
DATE_FORMAT = '%A, %B %-d, %Y'

MARKDOWN_EXTENSIONS = ['extra', 'codehilite', 'sane_lists', 'smarty']

# Other constants
MARKDOWN_REGEX = re.compile(r'(.*)\.(?:markdown|md)', re.IGNORECASE)
YAML_REGEX = re.compile(r'(.*)\.(?:yaml|yml)', re.IGNORECASE)

INDEX_TEMPLATE_MTIME = os.stat(INDEX_TEMPLATE_HTML_PATH).st_mtime
with open(INDEX_TEMPLATE_HTML_PATH, 'r') as fh:
    INDEX_TEMPLATE_HTML = fh.read()

ENTRY_TEMPLATE_MTIME = os.stat(ENTRY_TEMPLATE_HTML_PATH).st_mtime
with open(ENTRY_TEMPLATE_HTML_PATH, 'r') as fh:
    ENTRY_TEMPLATE_HTML = fh.read()

POST_TEMPLATE_MTIME = os.stat(POST_TEMPLATE_HTML_PATH).st_mtime
with open(POST_TEMPLATE_HTML_PATH, 'r') as fh:
    POST_TEMPLATE_HTML = fh.read()

# Structures
class Post:
    __slots__ = (
        'name',
        'markdown_path',
        'config_path',
        'output_path',
        'permalink',
        'metadata',
    )

    def __init__(self, name, md_path, cfg_path):
        self.name = name
        self.markdown_path = md_path
        self.config_path = cfg_path

        with open(cfg_path, 'r') as fh:
            self.metadata = yaml.safe_load(fh)

        date = self.metadata['date']
        blog_path = os.path.join(
            "{:04}".format(date.year),
            "{:02}".format(date.month),
            "{:02}".format(date.day),
            self.name + '.html',
        )
        self.output_path = os.path.join(OUTPUT_DIR, blog_path)
        self.permalink = os.path.join(BLOG_ROOT, blog_path)

    def needs_update(self):
        if not os.path.exists(self.output_path):
            return True

        cfg_mtime = os.stat(self.config_path).st_mtime
        md_mtime = os.stat(self.markdown_path).st_mtime
        out_mtime = os.stat(self.output_path).st_mtime

        in_mtime = max(cfg_mtime, md_mtime, INDEX_TEMPLATE_MTIME, ENTRY_TEMPLATE_MTIME, POST_TEMPLATE_MTIME)
        return in_mtime > out_mtime

    def generate(self):
        # Convert Markdown
        with open(self.markdown_path, 'r') as fh:
            md_html = markdown.markdown(
                fh.read(),
                extensions=MARKDOWN_EXTENSIONS,
                output_format='html5',
            )

        # Format HTML
        kwargs = self.metadata_dict()
        kwargs['content'] = md_html
        html = POST_TEMPLATE_HTML.format(**kwargs)

        # Write file
        os.makedirs(os.path.dirname(self.output_path), exist_ok=True)
        with open(self.output_path, 'w') as fh:
            fh.write(html)

    def metadata_dict(self):
        return {
            'title': self.metadata['title'],
            'description': self.metadata['description'],
            'keywords': ','.join(self.metadata['keywords']),
            'date': self.metadata['date'].strftime(DATE_FORMAT),
            'permalink': self.permalink,
        }

def write_index(number, posts, output_path, page_count):
    # Format HTML for each entry
    entry_html = '\n'.join([
        ENTRY_TEMPLATE_HTML.format(**post.metadata_dict())
        for post in posts
    ])

    # Format page links
    pages_parts = []
    for i in range(page_count):
        page = INDEX_NUMBER_TEMPLATE.format(i + 1)
        if i == number:
            pages_parts.append('<b>{}</b>'.format(page))
        else:
            pages_parts.append('<a href="{}">{}</a>'.format(
                INDEX_OUTPUT_FILENAME.format(i),
                page,
            ))

    # Format HTML for the full index
    start = INDEX_SIZE * number + 1
    html = INDEX_TEMPLATE_HTML.format(
        start=start,
        end=start + len(posts),
        body=entry_html,
        pages=' | '.join(pages_parts),
    )

    # Write final HTML
    with open(output_path, 'w') as fh:
        fh.write(html)

def chunks(iterable, size):
    it = iter(iterable)
    while True:
        chunk = tuple(itertools.islice(it, size))
        if chunk:
            yield chunk
        else:
            return

if __name__ == '__main__':
    # Parse arguments
    argparser = argparse.ArgumentParser(description='Generate HTML blog posts')
    argparser.add_argument('-q', '--quiet',
        dest='stdout', action='store_false',
        help="Don't output anything.")
    argparser.add_argument('-f', '--force',
        dest='force', action='store_true',
        help="Regenerate all files regardless of timestamps.")
    args = argparser.parse_args()

    def log(message):
        if args.stdout:
            print(message)

    # Change directory
    os.chdir(os.path.dirname(sys.argv[0]))

    # {name : [markdown_path, yaml_path]}
    path_tree = defaultdict(lambda: [None, None])

    # Generate path tree
    for filename in os.listdir(INPUT_DIR):
        match = MARKDOWN_REGEX.match(filename)
        if match is not None:
            name = match[1]
            path_tree[name][0] = os.path.join(INPUT_DIR, filename)
            continue

        match = YAML_REGEX.match(filename)
        if match is not None:
            name = match[1]
            path_tree[name][1] = os.path.join(INPUT_DIR, filename)
            continue

    # Check path tree
    posts = []
    for name, (md_path, cfg_path) in path_tree.items():
        if md_path is None or cfg_path is None:
            log("ERROR: Missing file for '{}'".format(name))
            exit(1)

        posts.append(Post(name, md_path, cfg_path))

    if not posts:
        log("ERROR: No input files.")
        exit(1)

    # Generate posts
    for post in posts:
        if post.needs_update() or args.force:
            log("HTML:  {}".format(post.output_path))
            post.generate()
        else:
            log("SKIP:  {}".format(post.output_path))
            continue

    # Sort posts by date
    posts.sort(key=lambda x: x.metadata['date'])
    posts.reverse()

    # Generate indices
    page_count = int(ceil(len(posts) / INDEX_SIZE))
    for idx, idx_posts in enumerate(chunks(posts, INDEX_SIZE)):
        output_path = os.path.join(OUTPUT_DIR, INDEX_OUTPUT_FILENAME.format(idx))
        log("INDEX: {}".format(output_path))
        write_index(idx, idx_posts, output_path, page_count)

    # Symlink final index
    index_zero = INDEX_OUTPUT_FILENAME.format(0)
    index_main = os.path.join(OUTPUT_DIR, INDEX_FILENAME)
    if os.path.exists(index_main):
        if args.force:
            os.remove(index_main)
    if not os.path.islink(index_main):
        log("LINK:  {} -> {}".format(index_zero, index_main))
        os.symlink(index_zero, index_main)

    # Finished
    log("DONE.")
